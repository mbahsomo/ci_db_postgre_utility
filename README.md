[CodeIgniter Postgresql Database Utility](https://bitbucket.org/mbahsomo/ci_db_postgre_utility)
========================

My custom build postgresql database backup utility for CodeIgniter.

## Usage:
Copy and replace file:  
```
system/database/drivers/postgre/postgre_utility.php
```
with (All Table)
```
$this->load->dbutil();

$backup = $this->dbutil->backup();
		
$this->load->helper('file');
write_file('assets/db/backup.gz', $backup);

		
$this->load->helper('download');
force_download('backup.gz', $backup);
```
or 
```
$this->load->dbutil();
		
$prefs = array(
	'tables'        => array('table1','table2'),   // Array of tables to backup.
	'ignore'        => array(),                     // List of tables to omit from the backup
	'format'        => 'txt',                       // gzip, zip, txt
	'filename'      => 'mybackup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
	'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
	'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
	'newline'       => "\n"                         // Newline character used in backup file
);
		
$backup = $this->dbutil->backup($prefs);
		
$this->load->helper('file');
write_file('assets/db/backup.sql', $backup);

		
$this->load->helper('download');
force_download('backup.sql', $backup);
```
## Reference:  
[CodeIgniter Database Utility User Guide](http://ellislab.com/codeigniter/user-guide/database/utilities.html#backup).

## Modif :
**mbahsomo**
Website: http://sugik.do-event.com  

## Idea:  
**Muhammad Nur Hidayat**  
Website: http://muhammadnurhidayat.com  
url : http://github.com/seebeb/ci_postgresql_db_utility